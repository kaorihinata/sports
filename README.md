# sports

sports (or Simple Ports), is a simple, POSIX shell based ports system that
aims to provide a simple, FreeBSD Ports-like solution for macOS (right now,
anyway) with as few dependencies as possible. Everything that can be written
in POSIX shell, will be, and if we need to use a tool from the base system
for the time being to accomplish a task, then so be it.

## The Anatomy of a Port

sports itself tries to make as few assumptions as possible. You must have,
at minimum, a `port.subr` file in the current directory to define the
stages of your port. Those stages are:

### `port_fetch`

In this stage you fetch any necessary distfiles, and verify their hashes
(if necessary.)

To simplify the process, we provide the `distfile` tool (see `distfile help`
for more details.) For example, let's write a port for our fictitious piece
of software, `mysoftware`:

```sh
port_fetch() {
    distfile fetch  \
        'https://example.com/mysoftware/mysoftware-0.1.2.txz' \
        'mysoftware-0.1.2.txz'
    distfile verify 'mysoftware-0.1.2.txz'
}
```

If you haven't yet generated a distinfo manifest, then `verify` will fail,
but the file you requested will be placed in the `work` directory. To
generate a distinfo manifest, use the `distinfo` tool (see `distinfo help`
for more details.) For example:

```sh
distinfo init 'work/mysoftware-0.1.2.txz'
```

This will generate a distinfo manifest next to your `port.subr` file.

### `port_extract`

In this stage you extract any necessary distfiles (if necessary.)

To simplify the process, the `distfile` tool can be used here as well.
Continuing from the above example:

```sh
port_extract() {
    distfile extract 'mysoftware-0.1.2.txz'
}
```

In most situations, archives will contain a single top level folder. Be
mindful of any other files which may exist in your work directory when you
extract (you would have to have put them there, after all.)

### `port_patch`

In this stage you apply any necessary patches (if necessary.) There are many
ways to patch source code. If you'd like any specific examples, then please
let me know.

### `port_configure`

In this stage you bootstrap and configure the build process (if necessary.)

Continuing from the above examples, let's say that `mysoftware` is a typical
piece of software for a Unix-like system which comes with a `configure`
script, so you could:

```sh
port_configure() {
    cd 'mysoftware-0.1.2'
    ./configure --prefix="${PREFIX}"
}
```

### `port_build`

In this stage you build the port (if necessary.) Not all ports require
building (like binary ports.)

Continuing from the above examples, let's (again) say that `mysoftware` is
a typical piece of software for a Unix-like system which uses some form of
`make` (BSD or GNU.) You could:

```sh
port_build() {
    cd 'mysoftware-0.1.2'
    make -j "$(sysctl -n hw.ncpu)"
}
```

### `port_install`

In this stage you install the port (if necessary.)

Continuing from the above examples, let's (again again) say that
`mysoftware` is a typical piece of software for a Unix-like system which
(still) uses some form of `make` (BSD or GNU.) You could:

```sh
port_install() {
    cd 'mysoftware-0.1.2'
    make install
}
```

Easy, but what if this isn't an option? What if you're installing something
written by an author that has opinions about how they think you should
install (or not install) their software? That's fine, let's just use the
`install` utility and do it our way:

```sh
port_install() {
    cd 'ihaveopinions-2.1.0'
    find bin lib \
        -type d -exec install -d "${PREFIX}/{}" \; \
        -type f -exec install "{}" "${PREFIX}/{}" \;
}
```

### `port_clean`

In this stage you cleanup the port to prepare for the destruction of the
work directory. *This is not typically necessary.* However, there are some
special cases (like with `go` packages) where file permissions may prevent
deletion. Mount points will have the same effect.

## Making the Port Maintainable

Now that you've written a port in the previous section, let's try to improve
it (ignoring the opinionated example, of course.) The port currently looks
like this:

```sh
#! /bin/sh -e

port_fetch() {
    distfile fetch  \
        'https://example.com/mysoftware/mysoftware-0.1.2.txz' \
        'mysoftware-0.1.2.txz'
    distfile verify 'mysoftware-0.1.2.txz'
}

port_extract() {
    distfile extract 'mysoftware-0.1.2.txz'
}

port_build() {
    cd 'mysoftware-0.1.2'
    make -j "$(sysctl -n hw.ncpu)"
}

port_install() {
    cd 'mysoftware-0.1.2'
    make install
}
```

In it's current state the port is just a collection of shell functions. You
have no explicit version, or even name information, so let's try adding that
to the top of the file:

```sh
PORTNAME='mysoftware'
PORTVERSION='0.1.2'
```

Great, and it follows the same naming scheme used in FreeBSD, so if we use
this naming scheme in our other ports as well, we can start to create a
reliable standard we can rely on when we start inheriting from other ports.
That's for later though.

For the time being, let's go one step further and add some aliases so that we
can type less.

```sh
PN="${PORTNAME}"
PV="${PORTVERSION}"
```

Even better. Now, you may have noticed that there are a lot of places where
the package name, and version are used throughout the port. Let's tighten
things up using some variable expansion:

```sh
port_fetch() {
    distfile fetch "https://example.com/${PN}/${PN}-${PV}.txz" \
                   "${PN}-${PV}.txz"
    distfile verify "${PN}-${PV}.txz"
}

port_extract() {
    distfile extract "${PN}-${PV}.txz"
}

port_build() {
    cd "${PN}-${PV}"
    make -j "$(sysctl -n hw.ncpu)"
}

port_install() {
    cd "${PN}-${PV}"
    make install
}
```

Alright! Now there isn't a single hard coded name or version in any function
at all. With this, bumping the package becomes as simple as changing the
version number. Your life as a maintainer is now a bit simpler. The complete
port now looks like this:

```sh
#! /bin/sh -e

PORTNAME='mysoftware'
PORTVERSION='0.1.2'
PN="${PORTNAME}"
PV="${PORTVERSION}"

port_fetch() {
    distfile fetch "https://example.com/${PN}/${PN}-${PV}.txz" \
                   "${PN}-${PV}.txz"
    distfile verify "${PN}-${PV}.txz"
}

port_extract() {
    distfile extract "${PN}-${PV}.txz"
}

port_build() {
    cd "${PN}-${PV}"
    make -j "$(sysctl -n hw.ncpu)"
}

port_install() {
    cd "${PN}-${PV}"
    make install
}
```
