#! /bin/sh

set -o errexit

if [ -z "$_SPORTSTAGE" ]; then
    test -n "$1" || set -- build
    for i in "$@"
    do
        case "$i" in
            all-depends-list|build|clean|configure|depends|depends-list|\
            extract|fetch|health|install|installed|patch|test)
                continue ;;
            help)
                cat >&2 <<-EOF
usage: sport {all-depends-list,build,clean,configure,depends,depends-list,
              extract,fetch,health,help,install,patch,test}...

Perform an action within a port directory. The commands are meant to
loosely resemble those supported by FreeBSD ports.

positional arguments:
  {all-depends-list,build,clean,configure,depends,depends-list,extract,
   fetch,health,help,install,patch,test}

    all-depends-list  list dependencies (recursively)
    build             fetch, extract, patch, configure and build the port
    clean             clean the port
    configure         fetch, extract, patch, and configure the port
    depends           fetch, extract, patch, configure, build, install, and
                      clean dependencies (recursively)
    depends-list      list dependencies
    extract           fetch, and extract the port
    fetch             fetch the port
    health            run a port health report (for maintainers)
    help              display this message and exit
    install           fetch, extract, patch, configure, build, and install
                      the port
    patch             fetch, extract, and patch the port
    test              test the port (for developers and maintainers)
				EOF
                exit 0 ;;
            *)
                printf '%s: error: invalid stage: %s\n' "${0##*/}" "$i" \
                    >&2
                exit 1 ;;
        esac
    done
    while [ -n "$1" ]; do
        case "$1" in
            # Steps without depends.
            fetch) set -- "$@" port_fetch ;;
            extract) set -- "$@" port_fetch port_extract ;;
            patch) set -- "$@" port_fetch port_extract port_patch ;;
            # Steps with depends.
            configure) set -- "$@" port_depends port_fetch port_extract \
                                   port_patch port_configure ;;
            build) set -- "$@" port_depends port_fetch port_extract \
                               port_patch port_configure port_build ;;
            install) set -- "$@" port_depends port_fetch port_extract \
                                 port_patch port_configure port_build \
                                 port_install ;;
            test) set -- "$@" port_depends port_fetch port_extract \
                              port_patch port_configure port_build \
                              port_test ;;
            # Standalone steps.
            all-depends-list) set -- "$@" port_all_depends_list ;;
            clean) set -- "$@" port_clean ;;
            depends) set -- "$@" port_depends ;;
            depends-list) set -- "$@" port_depends_list ;;
            health) set -- "$@" port_health ;;
            installed) set -- "$@" port_installed ;;
            *) break ;;
        esac
        shift
    done
    # Set a default or user defined PREFIX.
    _PREFIX="${PREFIX:-$HOME/.sandbox}"
    # Set CFLAGS if necessary.
    unset _CFLAGS
    _CFLAGS="$_CFLAGS -I$_PREFIX/include"
    _CFLAGS="${_CFLAGS## }"
    # Set LDFLAGS if necessary.
    unset _LDFLAGS
    _LDFLAGS="$_LDFLAGS -L$_PREFIX/lib"
    _LDFLAGS="${_LDFLAGS## }"
    # Set PKG_CONFIG_PATH if necessary.
    unset _PKG_CONFIG_PATH
    _PKG_CONFIG_PATH="$_PKG_CONFIG_PATH:$_PREFIX/lib/pkgconfig"
    _PKG_CONFIG_PATH="${_PKG_CONFIG_PATH##:}"
    # Sync HTTPS_PROXY and https_proxy (if necessary.)
    if [ -n "$https_proxy" ] && [ -z "$HTTPS_PROXY" ]; then
        HTTPS_PROXY="$https_proxy"
    fi
    # Set a default (and minimal) PATH.
    test -z "${0%%/*}" && _PATH="${0%/*}" || _PATH="$(pwd)/${0%/*}"
    _PATH="$_PATH:$_PREFIX/bin:$_PREFIX/sbin"
    _PATH="$_PATH:/bin:/sbin:/usr/bin:/usr/sbin"
    # Execute the requested stage or stages.
    for _SPORTSTAGE in "$@"; do
        env -i -- CFLAGS="$_CFLAGS" CPPFLAGS="$_CFLAGS" \
            CXXFLAGS="$_CFLAGS" HTTPS_PROXY="$HTTPS_PROXY" \
            LDFLAGS="$_LDFLAGS" OPTIONS="$OPTIONS" PATH="$_PATH" \
            PKG_CONFIG_PATH="$_PKG_CONFIG_PATH" PREFIX="$_PREFIX" \
            _SPORTSTAGE="$_SPORTSTAGE" http_proxy="$http_proxy" \
            https_proxy="$HTTPS_PROXY" "$0"
        shift
    done
    exit 0
fi

# Define the port path.
_SPORTPATH="$(pwd)"
# Define the friendly stage name (mostly used for matching.)
case "$_SPORTSTAGE" in
    port_depends_list) _SPORTSTAGENAME="depends-list" ;;
    port_all_depends_list) _SPORTSTAGENAME="all-depends-list" ;;
    *) _SPORTSTAGENAME="${_SPORTSTAGE#port_}" ;;
esac

readonly HTTPS_PROXY
readonly OPTIONS
readonly PREFIX
readonly _SPORTPATH
readonly _SPORTSTAGE
readonly _SPORTSTAGENAME
readonly http_proxy
readonly https_proxy

# Source the port to define custom stages.
if [ ! -f "$_SPORTPATH/port.subr" ]; then
    printf '%s: error: port.subr not found\n' "${0##*/}" >&2
    exit 1
fi
# shellcheck disable=SC1091
. "$_SPORTPATH/port.subr"
# Define the port name (if necessary.) The default port name is the parent
# directory.
test -n "$PORTNAME" || PORTNAME="${_SPORTPATH##*/}"
# Define the stage cookie (if necessary.)
_COOKIE="_done.$PORTNAME.$(printf "%s" "$PREFIX" | tr / _)"
case "$_SPORTSTAGENAME" in
    build|configure|depends|extract|fetch|install|patch)
        _COOKIE=".$_SPORTSTAGENAME$_COOKIE" ;;
    *) unset _COOKIE ;;
esac
# Skip the stage (if necessary.)
if [ -n "$_COOKIE" ] && [ -f "$_SPORTPATH/work/$_COOKIE" ]; then
     # This stage has already been completed.
     exit 0
fi
# Define the port category (if necessary.) The default port category is the
# parent directory of the parent directory.
if [ -z "$CATEGORY" ]; then
    CATEGORY="${_SPORTPATH%/*}"
    CATEGORY="${CATEGORY##*/}"
fi
# Define the receipt (if necessary.) This is only used at installation, or
# to determine if installation has already occurred.
case "$_SPORTSTAGENAME" in
    install|installed)
        _RECEIPT="$PREFIX/var/db/sports/$(printf '%s_%s' \
            "$CATEGORY" "$PORTNAME" | tr / _)" ;;
    *) unset _RECEIPT ;;
esac
# Execute necessary pre-stage/stage-less commands.
case "$_SPORTSTAGENAME" in
    all-depends-list)
        # Exit early if there are no dependencies to consider.
        test -n "$DEPENDS" || exit 0
        # Prime the list using the current port (needed to properly
        # handle OPTIONS.)
        set --
        while read -r _PORT; do
            set -- "$(cd "$_PORT";pwd)" "$@"
        done <<-EOF
			$DEPENDS
		EOF
        # Process the rest of the list. Could probably be more efficient
        # by checking if the current item is already in _RESULT, but we'll
        # see.
        unset _RESULT
        while [ -n "$1" ]; do
            _PORTS="$(cd "$1" && env -i OPTIONS="$OPTIONS" "$0" \
                      depends-list)"
            _RESULT="$1
$_RESULT"
            shift
            if [ -n "$_PORTS" ]; then
                while read -r _PORT
                do
                    set -- "$_PORT" "$@"
                done <<-EOF
					$_PORTS
				EOF
            fi
        done
        # Remove duplicates without order. sort/uniq wouldn't really do this
        # the way we want.
        awk '/^..*$/{if($0 in ports)next;print;ports[$0]=1;}' <<-EOF
			$_RESULT
		EOF
        exit 0 ;;
    clean)
        if [ ! -d "$_SPORTPATH/work" ]; then exit 0; fi
        cd "$_SPORTPATH/work" ;;
    build|configure|extract|patch|test)
        cd "$_SPORTPATH/work" ;;
    depends)
        mkdir -p work
        _PORTS="$(env -i OPTIONS="$OPTIONS" PREFIX="$PREFIX" "$0" \
            all-depends-list)"
        if [ -n "$_PORTS" ]; then
            while read -r _PORT; do
                (
                    cd "$_PORT"
                    if ! env -i PREFIX="$PREFIX" "$0" installed
                    then
                        env -i PREFIX="$PREFIX" HTTPS_PROXY="$HTTPS_PROXY" \
                            http_proxy="$http_proxy" \
                            https_proxy="$https_proxy" \
                            "$0" clean install clean
                    fi
                )
            done <<-EOF
				$_PORTS
			EOF
        fi
        touch "$_SPORTPATH/work/$_COOKIE"
        exit 0 ;;
    depends-list)
        if [ -n "$DEPENDS" ]; then
            printf '%s\n' "$DEPENDS" | tr -s '\n' | \
            while read -r _DEPEND; do (cd "$_DEPEND"; pwd); done
        fi
        exit 0 ;;
    fetch)
        mkdir -p work
        cd "$_SPORTPATH/work" ;;
    install)
        cd "$_SPORTPATH/work"
        mkdir -p "$PREFIX" ;;
    installed)
        test -f "$_RECEIPT" || exit 1
        exit 0 ;;
esac
# Execute necessary stage commands.
if command -v "$_SPORTSTAGE" >/dev/null 2>/dev/null
then
    export CFLAGS
    export CPPFLAGS
    export LDFLAGS
    export PKG_CONFIG_PATH
    export PREFIX
    printf '%s: entering %s stage for %s/%s\n' "${0##*/}" \
        "${_SPORTSTAGENAME}" "$CATEGORY" "$PORTNAME"
    "$_SPORTSTAGE"
    printf '%s: leaving %s stage for %s/%s\n' "${0##*/}" \
        "${_SPORTSTAGENAME}" "$CATEGORY" "$PORTNAME"
fi
# Execute necessary post-stage commands.
case "$_SPORTSTAGENAME" in
    clean)
        if [ -d "$_SPORTPATH/work" ]; then
            cd "$_SPORTPATH"
            rm -rf "$_SPORTPATH/work"
        fi ;;
    install)
        mkdir -p "$PREFIX/var/db/sports"
        printf '%s\n' "$PORTVERSION" >"$_RECEIPT" ;;
esac
# Create the stage cookie (if necessary.)
if [ -n "$_COOKIE" ] && [ ! -f "$_SPORTPATH/work/$_COOKIE" ]; then
    touch "$_SPORTPATH/work/$_COOKIE"
fi
